use {derive_more::Deref, serde::Deserialize, std::collections::HashMap, uuid::Uuid};

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ListId {
	#[serde(deserialize_with = "de_weird_array")]
	pub builds: Box<[Build]>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Build {
	/// Update title, for example Windows 10 Insider Preview 19577.1000
	/// (rs_prerelease)
	pub title: Box<str>,
	/// Update build number, for example 19577.1000
	pub build: Box<str>,
	// Update architecture
	pub arch: Arch,
	/// Timestamp of when the build was added to the database
	pub created: Option<u64>,
	/// UUID Update Identifier
	pub uuid: Uuid,
}

#[derive(Clone, Debug, Deref, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FetchUpd {
	#[serde(flatten)]
	#[deref]
	pub update: Update,
	pub update_array: Box<[Update]>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Update {
	/// UUID Update Identifier
	pub update_id: Uuid,
	/// Update title, such as Windows 10 Insider Preview 19577.1000
	/// (rs_prerelease)
	pub update_title: Box<str>,
	/// Update build number, such as 19577.1000
	pub found_build: Box<str>,
	/// Update architecture
	pub arch: Arch,
	/// Build database status
	pub file_write: FileWrite,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Get {
	pub update_name: Box<str>,
	pub arch: Arch,
	pub build: Box<str>,
	pub files: HashMap<Box<str>, GetFiles>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GetFiles {
	/// The file's SHA1 checksum
	pub sha1: Box<str>,
	/// File size in bytes
	pub size: u64,
	/// Data if `noLinks=1` not used
	#[serde(flatten)]
	pub link_extras: GetFilesLinksExtra,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GetFilesLinksExtra {
	/// File download link
	pub url: Box<str>,
	/// File UUID
	pub uuid: Uuid,
	/// Link expiration date
	pub expire: Box<str>,
	/// Raw data from Microsoft servers
	pub debug: Box<str>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ListLangs {
	/// Short language name, `xx-xx` format
	pub lang_list: Box<[Box<str>]>,
	/// key-value pairs of `xx-xx` to full language name
	#[serde(deserialize_with = "de_empty_array_is_default")]
	pub lang_fancy_names: HashMap<Box<str>, Box<str>>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ListEditions {
	/// Short edition name
	pub edition_list: Box<[Box<str>]>,
	/// key-value pairs of edition name to fancy edition name
	#[serde(deserialize_with = "de_empty_array_is_default")]
	pub edition_fancy_names: HashMap<Box<str>, Box<str>>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum FileWrite {
	/// Build was already in the database
	NoSave,
	/// Build was just added to the database
	InfoWritten,
}

/// Response JSON returned by the API
#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct JsonResponse<T> {
	/// Current JSON API version
	pub json_api_version: Box<str>,

	/// Response with UUP dump API version and specific data
	pub response: Response<T>,
}

/// Object with response and `api_version` flat-in (for some reason)
#[derive(Clone, Debug, Deref, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Response<T> {
	/// Current UUP dump API version
	pub api_version: Box<str>,

	/// Response-specific data
	#[serde(flatten)]
	#[deref]
	pub payload: T,
}

#[derive(Clone, Copy, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum Arch {
	Amd64,
	X86,
	Arm64,
}

fn de_empty_array_is_default<'de, D, T>(deserializer: D) -> Result<T, D::Error>
where
	T: Deserialize<'de> + Default,
	D: serde::Deserializer<'de>,
{
	#[derive(Deserialize)]
	#[serde(untagged)]
	enum ExpectedOrEmpty<T> {
		Expected(T),
		Empty([(); 0]),
	}

	let either = ExpectedOrEmpty::<T>::deserialize(deserializer)?;
	match either {
		ExpectedOrEmpty::Expected(valid) => Ok(valid),
		ExpectedOrEmpty::Empty(_) => Ok(Default::default()),
	}
}

fn de_weird_array<'de, D, T>(deserializer: D) -> Result<Box<[T]>, D::Error>
where
	D: serde::Deserializer<'de>,
	T: Deserialize<'de>,
{
	use std::marker::PhantomData;

	struct Visitor<T>(PhantomData<fn(T)>);
	impl<'de, T> serde::de::Visitor<'de> for Visitor<T>
	where
		T: Deserialize<'de>,
	{
		type Value = Box<[T]>;

		fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
			formatter.write_str("a map or a sequence")
		}

		fn visit_map<A>(self, mut map: A) -> Result<Self::Value, A::Error>
		where
			A: serde::de::MapAccess<'de>,
		{
			let capacity = map.size_hint().unwrap_or_default();
			let mut entries = Vec::with_capacity(capacity);
			while let Some((_, value)) = map.next_entry::<serde::de::IgnoredAny, T>()? {
				entries.push(value);
			}

			Ok(entries.into_boxed_slice())
		}

		fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
		where
			A: serde::de::SeqAccess<'de>,
		{
			let capacity = seq.size_hint().unwrap_or_default();
			let mut entries = Vec::with_capacity(capacity);
			while let Some(entry) = seq.next_element()? {
				entries.push(entry);
			}

			Ok(entries.into_boxed_slice())
		}
	}

	let visitor = Visitor::<T>(PhantomData);
	deserializer.deserialize_any(visitor)
}
