pub mod requests;
pub mod responses;

pub use requests::{fetchupd, get, list_editions, list_id, list_langs, version};

use derive_more::{Display, From};

pub type ApiResult<T> = Result<responses::JsonResponse<T>, Error>;

#[derive(Debug, Display, From)]
pub enum Error {
	#[display(fmt = "API error (code {}): \"{}\"", code, message)]
	Api {
		code: u16,
		message: Box<str>,
	},
	Ureq(Box<ureq::Error>),
	Io(std::io::Error),
}

impl std::error::Error for Error {}
impl From<ureq::Error> for Error {
	fn from(value: ureq::Error) -> Self {
		Self::Ureq(Box::new(value))
	}
}
