use {
	crate::responses::{FetchUpd, Get, ListEditions, ListId, ListLangs},
	derive_more::From,
	serde::Deserialize,
	uuid::Uuid,
};

macro_rules! define_requests {
	{
		$(
			$(#[$fn_meta:meta])*
			$name:ident
				$(required($($mandatory_ident:ident : $mandatory_ty:ty),* $(,)?))?
				$(optional($(
					$(#[$optional_meta:meta])*
					$optional_ident:ident : $optional_ty:ty),* $(,)?
				))?
				-> $return:ty
				=> $endpoint:expr;
		)*
	} => {
		::paste::paste! {
			$(
				#[derive(Clone)]
				pub struct [<$return Request>] {
					request: ::ureq::Request,
				}

				impl [<$return Request>] {
					$(
						$(
							$(#[$optional_meta])*
							pub fn [<$optional_ident:snake>](self, value: $optional_ty) -> Self {
								let request = self.request.query(
									::std::stringify!($optional_ident),
									&value.to_param(),
								);

								Self { request }
							}
						)*
					)?

					/// Sends a request to a server and get parsed response
					pub fn call(self) -> $crate::ApiResult<$return> {
						let response = self.request.call();
						match response {
							Ok(response) => {
								response
									.into_json()
									.map_err($crate::Error::from)
							}
							Err(::ureq::Error::Status(code, response)) => {
								let deserialized: ErrorJson = response
									.into_json()?;

								let message = deserialized.response.error;
								return Err($crate::Error::Api {
									code, message
								});
							}
							Err(e) => return Err(e.into()),
						}
					}
				}

				$(#[$fn_meta])*
				pub fn $name (
					$(
						$([<$mandatory_ident:snake>]: $mandatory_ty),*
					)?
				) -> [<$return Request>]
				{
					#[allow(unused_mut)]
					let mut request = ::ureq::get(
						::std::concat!("https://api.uupdump.net/", $endpoint),
					);

					$(
						$(
							request = request.query(
								::std::stringify!($mandatory_ident),
								&[<$mandatory_ident:snake>].to_param(),
							);
						)*
					)?

					[<$return Request>] { request }
				}
			)*
		}
	};
}

/// Returns versions of both JSON and UUP dump API
pub fn version() -> crate::ApiResult<()> {
	ureq::get("https://api.uupdump.net")
		.call()?
		.into_json()
		.map_err(crate::Error::from)
}

define_requests! {
	/// Returns a list of builds in the local database (like »Browse the list of
	/// known builds« on the website)
	list_id
		optional(
			/// Optional search query
			search: &str,
			/// Optional sorting results by creation date
			sortByDate: bool
		)
		-> ListId
		=> "listid.php";

	/// Fetches the latest builds from Windows Update servers using specified
	/// parameters. (like »Fetch the latest build« on the website)
	fetchupd
		optional(
			/// Specifies which architecture the API will return
			arch:   Arch,
			/// Specifies the channel (ring) the API uses when querying Windows
			/// Update servers
			ring:   Ring,
			/// Content (Flight) tye to use when fetching information
			flight: Flight,
			/// Build number to use by the API when fetching information
			build:  u32,
			/// SKU number to use when fetching information
			sku:    u8,
			/// Release type to use when fetching information for Windows Core OS
			/// (WCOS), e.g. Windows 10X
			type_:  Type
		)
		-> FetchUpd
		=> "fetchupd.php";

	/// Retrieves download links for specified Update ID and provides lists of
	/// ready to use UUP sets.
	///
	/// # Parameters
	/// `uuid`: Update identifier
	get
		required(id: Uuid)
		optional(
			/// Create UUP set for selected language
			lang   : &str,
			/// Create UUP set for the selected edition
			edition: &str,
			/// Do not retrieve download links for the created UUP set
			noLinks: bool,
		)
		-> Get
		=> "get.php";

	/// Lists available languages for the specified Update ID
	///
	/// # Parameters
	/// `id`: Optional Update identifier
	list_langs
		optional(id: Uuid)
		-> ListLangs
		=> "listlangs.php";

	/// Lists available editions for the specified Update ID
	///
	/// # Parameters
	/// `lang`: Generate edition list for the selected language
	list_editions
		required(lang: &str)
		optional(
			/// Optional update identifier
			id: Uuid
		)
		-> ListEditions
		=> "listeditions.php";
}

trait ToParam {
	type Str;

	fn to_param(&self) -> Self::Str;
}

impl<'a> ToParam for &'a str {
	type Str = &'a str;

	fn to_param(&self) -> Self::Str {
		self
	}
}

impl ToParam for Uuid {
	type Str = Box<str>;

	fn to_param(&self) -> Self::Str {
		self.as_hyphenated().to_string().into_boxed_str()
	}
}

impl ToParam for u8 {
	type Str = Box<str>;

	fn to_param(&self) -> Self::Str {
		self.to_string().into_boxed_str()
	}
}

impl ToParam for u32 {
	type Str = Box<str>;

	fn to_param(&self) -> Self::Str {
		self.to_string().into_boxed_str()
	}
}

impl ToParam for bool {
	type Str = &'static str;

	fn to_param(&self) -> Self::Str {
		match self {
			false => "0",
			true => "1",
		}
	}
}

#[derive(Clone, Copy, Debug, From)]
pub enum Arch {
	All,
	Specific(crate::responses::Arch),
}

impl ToParam for Arch {
	type Str = &'static str;

	fn to_param(&self) -> Self::Str {
		match self {
			Self::All => "all",
			Self::Specific(crate::responses::Arch::Amd64) => "amd64",
			Self::Specific(crate::responses::Arch::X86) => "x86",
			Self::Specific(crate::responses::Arch::Arm64) => "arm64",
		}
	}
}

#[derive(Clone, Copy, Debug)]
pub enum Ring {
	Dev,
	Beta,
	ReleasePreview,
	Retail,
}

impl ToParam for Ring {
	type Str = &'static str;

	fn to_param(&self) -> Self::Str {
		match self {
			Self::Dev => "WIF",
			Self::Beta => "WIS",
			Self::ReleasePreview => "RP",
			Self::Retail => "RETAIL",
		}
	}
}

#[derive(Clone, Copy, Debug)]
pub enum Flight {
	Mainline,
	Active,
	Skip,
	Current,
}

impl ToParam for Flight {
	type Str = &'static str;

	fn to_param(&self) -> Self::Str {
		match self {
			Self::Mainline => "Mainline",
			Self::Active => "Active",
			Self::Skip => "Skip",
			Self::Current => "Current",
		}
	}
}

#[derive(Clone, Copy, Debug)]
pub enum Type {
	Production,
	Test,
}

impl ToParam for Type {
	type Str = &'static str;

	fn to_param(&self) -> Self::Str {
		match self {
			Type::Production => "Production",
			Type::Test => "Test",
		}
	}
}

#[derive(Deserialize)]
struct ErrorJson {
	response: ErrorJsonInner,
}

#[derive(Deserialize)]
struct ErrorJsonInner {
	error: Box<str>,
}
