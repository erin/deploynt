# Contribution guidelines

> Welcome to the Deploy NT project. You are here because we want to best and you are it. So, who is ready to write some code!

— (paraphrasing Cave Johnson)

Of course, code contributions are not the only way to contribute. You can
work on artwork, test the software, write documentation, report issues, submit feature requests or involve in discussions.

Please, don't use the issue tracker for support questions. For that, use
project's Matrix channel: [#deploynt:erindesu.cz](https://matrix.to/#/#deploynt:erindesu.cz).

## Code of conduct
This project follows [GNOME Code of Conduct](https://conduct.gnome.org/).

## Flatpak policy
The only supported distribution format of Deploy NT is Flatpak. Please, tag issues non-reproducible
in Flatpak with `not-flatpak`. Note, that fixed issues may regress in the future as Deploy NT is only tested in Flatpak. 

## Issue reporting
Deploy NT uses GitLab issue tracker.

### Security issues
If you find a security vulnerability, mark the issue as **confidental**, so only the maintainers can see it.

### Reporting bugs
When reporting bug, it's recommended to use and follow our bug report templates.

If you are not using the Flatpak release, try to reproduce the bug in it first, as mentioned in above *Flatpak policy*.

For small issues, like:
- spelling / grammar fixes in the documentation
- typo correction
- comment clean ups
- changes to infrastructure files (CI, `.gitignore`)
- build system changes
- source tree clean ups and reorganizations

You should directrly open a merge request instead of filing a new issue.

### Feature requests and enhancements
- Describe the feature and what you are trying to achieve
- Prior art

If you have some design, mockup and sketches made, provide them.

## Getting started
First of all, unless you are a contributor with rights to the repository,
fork the repository. For that you will need a GNOME GitLab account.

We recommend using this commit format `<username>/<category>/<name>` where:

- `username` is your GitLab username
- `category` is a kind of your contribution:
	- `feat` for features
	- `fix` for bugfixes
	- `chore` for a maintainance
- `name` of your contribution

For a contribution by `catgirl-ballmer` to implement a ramdisk device in BCDEdit: `catgirl-ballmer/feat/bcdedit-ramdisk`.

## Contributing to libraries
Deploy NT project contains bunch of libraries. They are not strictly part
of the desktop application and their improvements aren't tied to the goals of it. The can be simply built using `cargo` and Flatpak policy **does not** apply to them.

## Building Deploy NT
You will need a flatpak-builder. This command will install dependencies from flathub and build 
Deploy NT into the `flatpak-build-dir` directory.

```console
flatpak run org.flatpak.Builder -- --install-deps-from=flathub flatpak-build-dir build-aux/cz.erindesu.DeployNT.Devel.json
```

Once you are done with working on your bug fix or feature, push it and
you can open a marge request where your changes will be pending for
review by Deploy NT maintainers.

## Code guidelines
- Please format your code with `cargo fmt` before submiting your code
- We encourage contributors to structure the code in modular way to try
  encode semantics in the typesystem.
- Panic only when reached code is a bug

### Code style
- Deploy NT uses tabs
- Rust uses `rustfmt` with custom rules, so running `cargo +nightly fmt` should be enough
- For other languages, align things in a block as a table with spaces, like this:
```rs
Box {
	styles      ["meow"]
	orientation : vertical;
	visible     : false;
}
```

## Resources
- [GNOME Human Interface Guidelines](https://developer.gnome.org/hig)
- [GNOME Installer Mockups](https://gitlab.gnome.org/Teams/Design/os-mockups/-/tree/master/installer)
- BCD Reverse engineering:
	- [Windows BCD Structure](/libs/bcdedit/Reverse%20Engineering/bcd.pdf)
	- [Windows BCDEdit Command Reference](/libs/bcdedit/Reverse%20Engineering/bcdedit_reff.pdf)

## Commit message styles

```plain
category(package): Short explanation of the commit.

Longer explanation explaining exactly what's changed, whether any
external or private interfaces changed, what bugs were fixed (with bug
tracker reference if applicable) and so forth. Be concise but not too
brief.

Closes #69
```

Categories:
- feat: Feature implementations
- fix: Fixes a bug
- chore: Small tweaks, formatting
- translation: Translations

Package field, in applicable describes which sub-package (eg. `bcdedit`) is used. For the GUI app, use `deploynt`.
If it is related to project-specific things like `README.md` or issue templates, use `meta`.

If your commit closes an issue, include `Closes #issue-number` at the end.

[Developers, developers, developers!](https://www.youtube.com/watch?v=rRm0NDo1CiY)
