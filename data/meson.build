glib_compile_schemas = find_program('glib-compile-schemas')

template_config = { 'app-id': app_id }

# Build desktop file
desktop_file = i18n.merge_file(
	type  : 'desktop',
	input : configure_file(
		input         : '@0@.desktop.in.in'.format(base_id),
		output        : '@BASENAME@',
		configuration : template_config,
	),
	output      : '@0@.desktop'.format(app_id),
	po_dir      : podir,
	install     : true,
	install_dir : datadir / 'applications',
)

desktop_file_validate = find_program('desktop-file-validate', required: false)
if desktop_file_validate.found()
	test(
		'Validate desktop file',
		desktop_file_validate,
		args    : [ desktop_file.full_path() ],
		depends : desktop_file,
	)
endif

# Create appdata (eg. for Software app listings)
appdata_file = i18n.merge_file(
	input: configure_file(
		input         : '@0@.metainfo.xml.in.in'.format(base_id),
		output        : '@BASENAME@',
		configuration : template_config,
	),
	output      : '@0@.metainfo.xml'.format(app_id),
	po_dir      : podir,
	install     : true,
	install_dir : datadir / 'metainfo',
)

appstreamcli = find_program('appstreamcli', required: false)
if appstreamcli.found()
	test(
		'Validate Appdata file',
		appstreamcli,
		args    : ['validate', '--no-net', '--explain', appdata_file.full_path()],
		depends : appdata_file,
	)
endif

# GSettings schema
configure_file(
	input         : '@0@.gschema.xml.in'.format(base_id),
	output        : '@0@.gschema.xml'.format(app_id),
	configuration : template_config,
	install       : true,
	install_dir   : datadir / 'glib-2.0' / 'schemas',
)

test(
	'Validate GSchema',
	glib_compile_schemas,
	args: ['--strict', '--dry-run', meson.current_build_dir()],
)

subdir('icons')

gnome.post_install()
