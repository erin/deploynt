# Deploy NT

A Windows Installer… on Linux?

## Features
- [ ] Windows Image Deploynment
- [ ] Automatic partitioning
- [ ] Bootloader install
- [ ] Initial setup
- [ ] Bootloader custom config

## Libraries
This repository also contains custom libraries libraries. They are platform-indepdenent.

- [bcdedit](libs/bcdedit) – Windows Boot Configuration Data editing
- [hivex](libs/hivex) ­– FFI bindings to the [hivex](https://libguestfs.org/hivex.3.html) library
- [uupdump-api](libs/uupdump-api) – Access to [UUP Dump API](https://git.uupdump.net/uup-dump/json-api)
- [wimlib](libs/wimlib) – FFI bindings to the [wimlib](https://wimlib.net) library

## Credits
- [gtk-rust-template](https://gitlab.gnome.org/World/Rust/gtk-rust-template) and 
  [Pika Backup](https://gitlab.gnome.org/World/pika-backup) partially used to write build files

