#!/usr/bin/env bash

APPID="cz.erindesu.DeployNT"

cd "$(dirname "$0")"

jq -f /dev/stdin $APPID.json > $APPID.Devel.json << EOF
	.id += ".Devel" |
	.modules[-1]."config-opts" = ["-Dprofile=dev"] |
	."finish-args" += [
		"--env=RUST_BACKTRACE=1",
		"--env=G_MESSAGES_DEBUG=deploynt"
	]
EOF
