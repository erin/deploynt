use {
	adw::{gio, gio::prelude::*},
	color_eyre::{eyre::bail, Result},
	wimlib::{OpenFlags, Wim},
};

pub fn try_open_wim(wimlib: &wimlib::WimLib, file: &gio::File) -> Result<Option<Wim>> {
	let t_str = unsafe {
		let ptr = adw::gio::ffi::g_file_get_path(file.as_ptr());
		if ptr.is_null() {
			panic!("No file path found");
		}

		wimlib::string::TStr::from_ptr(ptr)
	};

	match wimlib.open_wim(t_str, OpenFlags::CHECK_INTEGRITY) {
		Ok(wim) => Ok(Some(wim)),
		Err(wimlib::Error::Open) => Ok(None),
		Err(e) => bail!(e),
	}
}
