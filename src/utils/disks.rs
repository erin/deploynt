use {
	udisks2::{block::BlockProxy, drive::DriveProxy},
	zbus::zvariant::OwnedObjectPath,
};

fn is_block_device_path(key: &OwnedObjectPath) -> bool {
	key.as_str()
		.starts_with("/org/freedesktop/UDisks2/block_devices/")
}

pub async fn get_block_for_drive<'p>(
	ud2: &udisks2::Client,
	drive: &DriveProxy<'p>,
) -> color_eyre::Result<Option<BlockProxy<'p>>> {
	let drive_path = drive.inner().path();
	let objects = ud2.object_manager().get_managed_objects().await?;

	let block_devices = objects.into_keys().filter(is_block_device_path);
	for block_device_path in block_devices {
		let obj = ud2.object(block_device_path)?;
		let block_device = obj.block().await?;

		// Check if block device belongs to specified drive
		if block_device.drive().await.as_deref() != Ok(drive_path) {
			continue;
		}

		// It's a partition → not toplevel
		if obj.partition().await.is_ok() {
			continue;
		}

		return Ok(Some(block_device));
	}

	// None was found
	Ok(None)
}
