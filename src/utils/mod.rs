use crate::prelude::*;

pub mod disks;
pub mod logging;
pub mod wim;

macro_rules! once_static {
	($ty:ty, $ctor:expr) => {{
		static STATIC: ::std::sync::OnceLock<$ty> = ::std::sync::OnceLock::new();

		STATIC.get_or_init(|| $ctor)
	}};
}

#[inline(always)]
pub fn default<T: Default>() -> T {
	T::default()
}

pub fn ancestor<T: IsA<gtk::Widget>>(child: &impl IsA<gtk::Widget>) -> Option<T> {
	child.ancestor(T::static_type()).and_downcast::<T>()
}

pub(crate) use once_static;
