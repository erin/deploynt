macro_rules! log {
	($level:expr, $($argv:tt)+) => {
		::adw::glib::g_log!(
			crate::consts::PACKAGE_NAME,
			$level,
			"[{}] {}",
			module_path!(),
			format_args!($($argv)+),
		)
	};
}

macro_rules! error {
	($($argv:tt)+) => {
		$crate::utils::logging::log!(
			::adw::glib::LogLevel::Critical,
			$($argv)+
		);
	}
}

macro_rules! warn_ {
	($($argv:tt)+) => {
		$crate::utils::logging::log!(
			::adw::glib::LogLevel::Warning,
			$($argv)+
		);
	};
}

macro_rules! info {
	($($argv:tt)+) => {
		$crate::utils::logging::log!(
			::adw::glib::LogLevel::Info,
			$($argv)+
		);
	};
}

macro_rules! debug {
	($($argv:tt)+) => {
		$crate::utils::logging::log!(
			::adw::glib::LogLevel::Debug,
			$($argv)+
		);
	};
}

pub(crate) use {debug, error, info, log, warn_ as warn};
