//! Application header bar

use crate::prelude::*;

mod imp {
	use super::*;

	#[derive(Debug, Default, gtk::CompositeTemplate)]
	#[template(file = "src/ui/headerbar.blp")]
	pub struct HeaderBar {}

	#[glib::object_subclass]
	impl ObjectSubclass for HeaderBar {
		const NAME: &'static str = "DntHeaderBar";
		type Type = super::HeaderBar;
		type ParentType = adw::Bin;

		fn class_init(class: &mut Self::Class) {
			class.bind_template();
		}

		fn instance_init(obj: &gtk::glib::subclass::InitializingObject<Self>) {
			obj.init_template();
		}
	}

	impl ObjectImpl for HeaderBar {}
	impl WidgetImpl for HeaderBar {}
	impl BinImpl for HeaderBar {}
}

glib::wrapper! {
	pub struct HeaderBar(ObjectSubclass<imp::HeaderBar>)
		@extends adw::Bin, gtk::Widget,
		@implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}
