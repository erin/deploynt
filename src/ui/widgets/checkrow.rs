//! [`adw::ActionRow`] with a checkbutton

use adw::{glib, subclass::prelude::*};

mod imp {
	use super::*;

	#[derive(Debug, Default, gtk::CompositeTemplate)]
	#[template(file = "src/ui/widgets/checkrow.blp")]
	pub struct CheckRow {
		#[template_child]
		check_button: TemplateChild<gtk::CheckButton>,
	}

	#[glib::object_subclass]
	impl ObjectSubclass for CheckRow {
		const NAME: &'static str = "DntCheckRow";
		type Type = super::CheckRow;
		type ParentType = adw::ActionRow;

		fn class_init(class: &mut Self::Class) {
			class.bind_template();
		}

		fn instance_init(obj: &gtk::glib::subclass::InitializingObject<Self>) {
			obj.init_template();
		}
	}

	impl ObjectImpl for CheckRow {}
	impl WidgetImpl for CheckRow {}
	impl ListBoxRowImpl for CheckRow {}
	impl PreferencesRowImpl for CheckRow {}
	impl ActionRowImpl for CheckRow {}
}

glib::wrapper! {
	/// [`adw::ActionRow`] with a checkbutton
	pub struct CheckRow(ObjectSubclass<imp::CheckRow>)
		@extends adw::ActionRow, adw::PreferencesRow, gtk::ListBoxRow, gtk::Widget,
		@implements gtk::Accessible, gtk::Actionable, gtk::Buildable, gtk::ConstraintTarget;
}

impl Default for CheckRow {
	fn default() -> Self {
		glib::Object::new()
	}
}
