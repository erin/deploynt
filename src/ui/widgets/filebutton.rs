//! Button to choose a file

use {
	crate::prelude::*,
	crate::{app::App, utils::once_static},
	color_eyre::Result,
	glib::{subclass::Signal, MainContext},
	std::cell::{Cell, RefCell},
};

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, glib::Enum, glib::Variant)]
#[variant_enum(enum)]
#[enum_type(name = "DntFileButtonSelectionType")]
pub enum SelectionType {
	#[default]
	File,
	Folder,
}

mod imp {
	use super::*;

	#[derive(Default, glib::Properties, gtk::CompositeTemplate)]
	#[template(file = "src/ui/widgets/filebutton.blp")]
	#[properties(wrapper_type = super::FileButton)]
	pub struct FileButton {
		/// File dialog itself
		#[property(get, set)]
		file_dialog: RefCell<gtk::FileDialog>,

		/// Whatever button selects file or folder
		#[property(
			get,
			set = Self::set_selection_type,
			builder(Default::default()))
		]
		selection_type: Cell<SelectionType>,

		/// Currently selected file
		#[property(get, nullable)]
		selected_file: RefCell<Option<gio::File>>,

		#[template_child]
		file_icon: TemplateChild<gtk::Image>,
		#[template_child]
		file_label: TemplateChild<gtk::Label>,

		#[template_child]
		stack: TemplateChild<gtk::Stack>,
		#[template_child]
		default_page: TemplateChild<gtk::Label>,
		#[template_child]
		selected_page: TemplateChild<gtk::Box>,
	}

	#[glib::object_subclass]
	impl ObjectSubclass for FileButton {
		const NAME: &'static str = "DntFileButton";
		type Type = super::FileButton;
		type ParentType = gtk::Button;

		fn class_init(class: &mut Self::Class) {
			class.bind_template();
			class.bind_template_callbacks();
		}

		fn instance_init(obj: &gtk::glib::subclass::InitializingObject<Self>) {
			obj.init_template();
		}
	}

	#[glib::derived_properties]
	impl ObjectImpl for FileButton {
		fn signals() -> &'static [Signal] {
			once_static!(
				[Signal; 1],
				[Signal::builder("file-opened")
					.param_types([gio::File::static_type()])
					.build()]
			)
		}
	}

	impl WidgetImpl for FileButton {}
	impl ButtonImpl for FileButton {}

	#[gtk::template_callbacks]
	impl FileButton {
		#[template_callback]
		fn on_clicked(&self) {
			// What type do it selects?
			let function = match self.selection_type.get() {
				SelectionType::File => gtk::FileDialog::open_future,
				SelectionType::Folder => gtk::FileDialog::select_folder_future,
			};

			let dialog = self.file_dialog.borrow().clone();
			let parent = App::current().main_window();

			let obj = self.obj().clone();
			MainContext::default().spawn_local(async move {
				// Ignore errors when user dismissed or cancelled the dialog
				let can_ignore_error = |error: &glib::Error| {
					matches!(
						error.kind::<gtk::DialogError>(),
						Some(gtk::DialogError::Dismissed | gtk::DialogError::Cancelled),
					)
				};

				// Open dialog and set
				let file_result = function(&dialog, Some(&parent)).await;
				match file_result {
					Ok(file) => {
						obj.imp().set_selected_file(file).await.unwrap();
					}
					Err(e) if can_ignore_error(&e) => (),
					Err(e) => panic!("{e}"),
				}
			});
		}
	}

	impl FileButton {
		/// Set selected file, update icon
		async fn set_selected_file(&self, file: gio::File) -> Result<()> {
			// Get file's icon and display name
			let info = file
				.query_info_future(
					"standard::icon,standard::display-name",
					gio::FileQueryInfoFlags::NONE,
					glib::Priority::DEFAULT,
				)
				.await?;

			let name = info.display_name();

			// If icon present – set, otherwise set a placeholder
			if let Some(icon) = info.icon() {
				self.file_icon.set_from_gicon(&icon);
			} else {
				self.file_icon.set_icon_name(Some("image-missing-symbolic"));
			}

			self.file_label.set_label(&name);
			self.stack.set_visible_child(&*self.selected_page);

			*self.selected_file.borrow_mut() = Some(file);
			self.obj().notify_selected_file();
			Ok(())
		}

		/// Remove file selection
		fn clear_selected_file(&self) {
			*self.selected_file.borrow_mut() = None;
			self.stack.set_visible_child(&*self.default_page);
		}

		/// Set if button selects file or directory
		fn set_selection_type(&self, selection_type: SelectionType) {
			self.clear_selected_file();
			self.selection_type.set(selection_type);
		}
	}
}

glib::wrapper! {
	/// Button to choose file or folder, with a nice icon and name inside
	pub struct FileButton(ObjectSubclass<imp::FileButton>)
		@extends gtk::Button, gtk::Widget,
		@implements gtk::Accessible, gtk::Actionable, gtk::Buildable, gtk::ConstraintTarget;
}
