//! Pre-production software warning page

use crate::prelude::*;
use crate::ui::wizard::wizard_page::WizardPage;

mod imp {
	use super::*;

	#[derive(Debug, Default, gtk::CompositeTemplate)]
	#[template(file = "src/ui/wizard/warning.blp")]
	pub struct Warning {}

	#[glib::object_subclass]
	impl ObjectSubclass for Warning {
		const NAME: &'static str = "DntWizardWarn";
		type Type = super::Warning;
		type ParentType = WizardPage;

		fn class_init(class: &mut Self::Class) {
			class.bind_template();
		}

		fn instance_init(obj: &gtk::glib::subclass::InitializingObject<Self>) {
			obj.init_template();
		}
	}

	impl ObjectImpl for Warning {}
	impl WidgetImpl for Warning {}
	impl NavigationPageImpl for Warning {}
}

glib::wrapper! {
	/// Pre-production software warning page
	pub struct Warning(ObjectSubclass<imp::Warning>)
		@extends WizardPage, adw::NavigationPage, gtk::Widget,
		@implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}
