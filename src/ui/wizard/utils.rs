use crate::app::App;

/// Throw a toast
pub fn throw_toast(toast: adw::Toast) {
	App::current()
		.main_window()
		.toast_overlay()
		.add_toast(toast);
}

/// Throw a simple toast with text message
pub fn throw_toast_msg(msg: &str) {
	let toast = adw::Toast::new(msg);
	throw_toast(toast);
}
