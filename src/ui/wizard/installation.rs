use super::wizard_page::WizardPage;
use crate::models::disk::Disk;
use crate::models::ImageChoice;
use crate::prelude::*;
use std::cell::OnceCell;

mod imp {
	use super::*;

	#[derive(Default, gtk::CompositeTemplate, glib::Properties)]
	#[template(file = "src/ui/wizard/installation.blp")]
	#[properties(wrapper_type = super::Installation)]
	pub struct Installation {
		#[property(get, construct_only)]
		disk: OnceCell<Disk>,
		#[property(get, construct_only)]
		image_choice: OnceCell<ImageChoice>,
	}

	#[glib::object_subclass]
	impl ObjectSubclass for Installation {
		const NAME: &'static str = "DntWizardInstallation";
		type Type = super::Installation;
		type ParentType = WizardPage;

		fn class_init(class: &mut Self::Class) {
			class.bind_template();
		}

		fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
			obj.init_template();
		}
	}

	impl ObjectImpl for Installation {}
	impl WidgetImpl for Installation {}
	impl NavigationPageImpl for Installation {}

	impl Installation {
		pub fn start(&self) {}
	}
}

glib::wrapper! {
	pub struct Installation(ObjectSubclass<imp::Installation>)
		@extends WizardPage, adw::NavigationPage, gtk::Widget,
		@implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl Installation {
	pub fn new() -> Self {
		glib::Object::new()
	}

	pub fn start(&self) {
		self.imp().start();
	}
}
