//! Installation wizard

mod source;
mod source_local;
mod target;
mod utils;
mod warning;
mod wizard_page;
mod installation;

use {crate::models::ImageChoice, crate::prelude::*, wizard_page::WizardPage};

mod imp {
	use super::*;

	#[derive(Debug, Default, glib::Properties, gtk::CompositeTemplate)]
	#[template(file = "src/ui/wizard/mod.blp")]
	#[properties(wrapper_type = super::Wizard)]
	pub struct Wizard {
		#[property(get)]
		image_choice: ImageChoice,

		#[template_child]
		navigation_view: TemplateChild<adw::NavigationView>,
		#[template_child]
		next_button: TemplateChild<gtk::Button>,
	}

	#[glib::object_subclass]
	impl ObjectSubclass for Wizard {
		const NAME: &'static str = "DntWizard";
		type Type = super::Wizard;
		type ParentType = adw::NavigationPage;

		fn class_init(class: &mut Self::Class) {
			wizard_page::WizardPage::ensure_type();
			warning::Warning::ensure_type();
			source::Source::ensure_type();
			source_local::SourceLocal::ensure_type();
			target::Target::ensure_type();
			target::data_detected::DataDetected::ensure_type();

			crate::models::ImageChoice::ensure_type();
			crate::ui::widgets::CheckRow::ensure_type();

			class.bind_template();
			class.bind_template_callbacks();
		}

		fn instance_init(obj: &gtk::glib::subclass::InitializingObject<Self>) {
			obj.init_template();
		}
	}

	#[glib::derived_properties]
	impl ObjectImpl for Wizard {
		fn constructed(&self) {
			self.parent_constructed();

			self.navigation_view
				.property_expression("visible-page")
				.chain_property::<WizardPage>("next-button-policy")
				.watch(
					glib::Object::NONE,
					glib::clone!(
						#[strong(rename_to = obj)]
						self.obj(),
						move || {
							obj.imp().update_next_button_state();
						}
					),
				);
		}
	}

	impl WidgetImpl for Wizard {}
	impl NavigationPageImpl for Wizard {
		fn showing(&self) {
			self.parent_showing();
			self.update_next_button_state();
		}
	}

	#[gtk::template_callbacks]
	impl Wizard {
		#[template_callback]
		fn on_next_button_clicked(&self) {
			// Can be changed only if a visible page exists
			let Some(visible_page) = self.navigation_view.visible_page() else {
				warn!("No visible page found");
				return;
			};

			// Get next page tag
			let tag = match visible_page
				.downcast::<WizardPage>()
				.map(|page| page.next_page())
			{
				Ok(Some(tag)) => tag,
				Ok(None) => {
					warn!("No next page found but next button policy is set to `Enabled`");
					return;
				}
				Err(_) => {
					error!("This page is not `WizardPage` and doesn't have next page set");
					return;
				}
			};

			self.navigation_view.push_by_tag(&tag);
		}

		#[template_callback]
		fn on_back_button_clicked(&self) {
			self.navigation_view.pop();
		}
	}

	impl Wizard {
		/// Update `next_button`'s state by page's policy
		fn update_next_button_state(&self) {
			// There has to be visible page to get state from
			let Some(visible_page) = self.navigation_view.visible_page() else {
				warn!("No visible page found");
				return;
			};

			let visible_page = visible_page
				.downcast::<WizardPage>()
				.expect("Expected WizardPage");

			let (visible, sensitive) = match visible_page.next_button_policy() {
				wizard_page::NextButtonPolicy::Disabled => (true, false),
				wizard_page::NextButtonPolicy::Enabled => (true, true),
				wizard_page::NextButtonPolicy::Hidden => (false, false),
			};

			self.next_button.set_visible(visible);
			self.next_button.set_sensitive(sensitive);
		}
	}
}

glib::wrapper! {
	pub struct Wizard(ObjectSubclass<imp::Wizard>)
		@extends adw::NavigationPage, gtk::Widget,
		@implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}
