//! Wizard page class

use {
	crate::prelude::*,
	std::cell::{Cell, RefCell},
};

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, glib::Enum, glib::Variant)]
#[variant_enum(enum)]
#[enum_type(name = "DntWizardNextButtonPolicy")]
pub enum NextButtonPolicy {
	/// Next button is visible, but not sensitive (default)
	#[default]
	Disabled,
	/// Next button is visible and sensitive
	Enabled,
	/// Next button will be hidden. It's up to the page to
	/// navigate to next page.
	Hidden,
}

mod imp {
	use super::*;

	#[derive(Debug, Default, glib::Properties, gtk::CompositeTemplate)]
	#[properties(wrapper_type = super::WizardPage)]
	#[template(file = "src/ui/wizard/wizard-page.blp")]
	pub struct WizardPage {
		#[template_child]
		inner: TemplateChild<gtk::Box>,

		#[property(get, set, builder(default()))]
		next_button_policy: Cell<NextButtonPolicy>,

		#[property(get, set)] // NOTE: Not a fan of owning here
		next_page: RefCell<Option<String>>,
	}

	#[glib::object_subclass]
	impl ObjectSubclass for WizardPage {
		const NAME: &'static str = "DntWizardPage";
		type Type = super::WizardPage;
		type ParentType = adw::NavigationPage;
		type Interfaces = (gtk::Buildable,);

		fn class_init(class: &mut Self::Class) {
			class.bind_template();
		}

		fn instance_init(obj: &gtk::glib::subclass::InitializingObject<Self>) {
			obj.init_template();
		}
	}

	#[glib::derived_properties]
	impl ObjectImpl for WizardPage {}
	impl WidgetImpl for WizardPage {}
	impl NavigationPageImpl for WizardPage {}

	impl BuildableImpl for WizardPage {
		fn add_child(&self, builder: &gtk::Builder, child: &glib::Object, type_: Option<&str>) {
			if !self.inner.is_bound() {
				self.parent_add_child(builder, child, type_);
			} else {
				self.inner
					.append(child.downcast_ref::<gtk::Widget>().unwrap());
			}
		}
	}
}

glib::wrapper! {
	/// Wizard page class
	///
	/// Has properties like
	/// - `next-button-policy`: defines if the next button should be sensitive/visible
	/// - `next-page`: following page (if `next-button-policy` = [`NextButtonPolicy::Enabled`])
	pub struct WizardPage(ObjectSubclass<imp::WizardPage>)
		@extends adw::NavigationPage, gtk::Widget,
		@implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

unsafe impl<T> IsSubclassable<T> for WizardPage where T: NavigationPageImpl {}
