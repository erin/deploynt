//! Source type selection page

use {
	crate::ui::wizard::wizard_page::WizardPage,
	crate::prelude::*,
};

mod imp {
	use super::*;

	#[derive(Debug, Default, gtk::CompositeTemplate)]
	#[template(file = "src/ui/wizard/source.blp")]
	pub struct Source {}

	#[glib::object_subclass]
	impl ObjectSubclass for Source {
		const NAME: &'static str = "DntWizardSource";
		type Type = super::Source;
		type ParentType = WizardPage;

		fn class_init(class: &mut Self::Class) {
			class.bind_template();
		}

		fn instance_init(obj: &gtk::glib::subclass::InitializingObject<Self>) {
			obj.init_template();
		}
	}

	impl ObjectImpl for Source {}
	impl WidgetImpl for Source {}
	impl NavigationPageImpl for Source {}
}

glib::wrapper! {
	/// Source type selection page
	pub struct Source(ObjectSubclass<imp::Source>)
		@extends WizardPage, adw::NavigationPage, gtk::Widget,
		@implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}
