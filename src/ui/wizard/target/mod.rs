pub mod data_detected;
mod dialogs;
mod disk_row;

use {
	super::wizard_page::WizardPage, crate::models::disk::Disk, crate::prelude::*,
	color_eyre::Result, disk_row::DiskRow, glib::MainContext, std::cell::OnceCell,
};

mod imp {
	use super::*;

	#[derive(Debug, Default, gtk::CompositeTemplate)]
	#[template(file = "src/ui/wizard/target/mod.blp")]
	pub struct Target {
		#[template_child]
		usable_disks: TemplateChild<gtk::ListBox>,
		#[template_child]
		unusable_disks: TemplateChild<gtk::ListBox>,
		#[template_child]
		unusable_disks_group: TemplateChild<adw::PreferencesGroup>,

		// If ListModels fall out of scope, they get deallocated
		// and signals will no longer get dispatched.
		_usable_disks_observer: OnceCell<gio::ListModel>,
		_unusable_disks_observer: OnceCell<gio::ListModel>,
	}

	#[glib::object_subclass]
	impl ObjectSubclass for Target {
		const NAME: &'static str = "DntWizardTarget";
		type Type = super::Target;
		type ParentType = WizardPage;

		fn class_init(class: &mut Self::Class) {
			class.bind_template();
		}

		fn instance_init(obj: &gtk::glib::subclass::InitializingObject<Self>) {
			obj.init_template();
		}
	}

	impl ObjectImpl for Target {
		fn constructed(&self) {
			self.parent_constructed();

			self.bind_items_changed(
				&*self.usable_disks,
				self.usable_disks.clone(),
				&self._usable_disks_observer,
			);

			self.bind_items_changed(
				&*self.unusable_disks,
				self.unusable_disks_group.clone(),
				&self._unusable_disks_observer,
			);
		}
	}

	impl WidgetImpl for Target {}
	impl NavigationPageImpl for Target {
		fn showing(&self) {
			MainContext::default().block_on(self.enumerate_disks());
		}
	}

	#[gtk::template_callbacks]
	impl Target {}

	impl Target {
		fn bind_items_changed(
			&self,
			widget: &impl WidgetExt,
			hider: impl WidgetExt,
			observer_cell: &OnceCell<gio::ListModel>,
		) {
			let observer = widget.observe_children();
			observer.connect_items_changed(move |self_, _, _, _| {
				let is_not_empty = self_.n_items() != 0;
				hider.set_visible(is_not_empty);
			});

			if observer_cell.set(observer).is_err() {
				unreachable!("Tried to bind observer twice");
			}
		}

		async fn enumerate_disks(&self) -> Result<()> {
			// Erase disk list
			self.usable_disks.remove_all();
			self.unusable_disks.remove_all();

			let ud2 = udisks2::Client::new().await?;

			// hack?: why allocate all objects for UDisks2 when we only need the drives?
			let drives = ud2
				.object_manager()
				.get_managed_objects()
				.await?
				.into_keys()
				.filter(|key| key.as_str().starts_with("/org/freedesktop/UDisks2/drives/"));

			for drive in drives {
				let drive = ud2.object(drive)?.drive().await?;

				let required_space = 0; // todo
				let disk = Disk::new(&ud2, &drive, required_space).await?;

				let is_disk_usable = disk.unusable().is_none();
				let row = DiskRow::new(disk);

				if is_disk_usable {
					self.usable_disks.append(&row);
				} else {
					self.unusable_disks.append(&row);
				}
			}

			Ok(())
		}
	}
}

glib::wrapper! {
	pub struct Target(ObjectSubclass<imp::Target>)
		@extends WizardPage, adw::NavigationPage, gtk::Widget,
		@implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}
