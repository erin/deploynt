use {
	super::disk_row::DiskRow,
	crate::prelude::*,
	crate::{
		models::{disk::Disk, ImageChoice, PartitioningChoice},
		ui::wizard::wizard_page::WizardPage,
	},
	gio::{SimpleAction, SimpleActionGroup},
	std::cell::{Cell, OnceCell},
};

mod imp {
	use super::*;

	#[derive(Debug, Default, gtk::CompositeTemplate, glib::Properties)]
	#[properties(wrapper_type = super::DataDetected)]
	#[template(file = "src/ui/wizard/target/data-detected.blp")]
	pub struct DataDetected {
		#[template_child]
		pub(super) disk_row_group: gtk::TemplateChild<adw::PreferencesGroup>,
		#[template_child]
		pub(super) label: gtk::TemplateChild<gtk::Label>,

		#[template_child]
		pub button: gtk::TemplateChild<gtk::Button>,

		choice: Cell<Option<PartitioningChoice>>,
		action_group: SimpleActionGroup,
	}

	#[glib::object_subclass]
	impl ObjectSubclass for DataDetected {
		const NAME: &'static str = "DntWizardTargetDataDetected";
		type Type = super::DataDetected;
		type ParentType = WizardPage;

		fn class_init(class: &mut Self::Class) {
			super::super::disk_row::DiskRow::ensure_type();
			class.bind_template();
			class.bind_template_callbacks();
		}

		fn instance_init(obj: &gtk::glib::subclass::InitializingObject<Self>) {
			obj.init_template();
		}
	}

	#[glib::derived_properties]
	impl ObjectImpl for DataDetected {
		fn constructed(&self) {
			self.parent_constructed();
			let choice_type = Option::<PartitioningChoice>::static_variant_type();
			let choice = SimpleAction::new_stateful(
				"choice",
				Some(&choice_type),
				&None::<PartitioningChoice>.to_variant(),
			);

			choice.connect_change_state(glib::clone!(
				#[strong(rename_to = obj)]
				self.obj(),
				move |self_, state| {
					let state_variant = state.expect("This action is supposed to have state");
					let choice = state_variant
						.get::<Option<PartitioningChoice>>()
						.expect("Expected `Option<PartitioningChoice>` variant type");

					let imp = obj.imp();
					imp.button.set_sensitive(choice.is_some());

					imp.choice.set(choice);
					self_.set_state(state_variant);
				}
			));

			self.action_group.add_action(&choice);

			self.obj()
				.insert_action_group("data-detected", Some(&self.action_group));
		}
	}

	impl WidgetImpl for DataDetected {}
	impl NavigationPageImpl for DataDetected {}

	#[gtk::template_callbacks]
	impl DataDetected {
		#[template_callback]
		fn on_install_clicked(&self) {
			let ctx = glib::MainContext::default();
			match self.choice.get().unwrap() {
				PartitioningChoice::FreeSpace => {
					ctx.spawn_local(super::super::dialogs::begin_installation());
				}
				PartitioningChoice::Wipe => {
					ctx.spawn_local(super::super::dialogs::begin_installation_danger());
				}
			}
		}
	}
}

glib::wrapper! {
	/// Data was detected on the disk, but there is still
	/// space available.
	///
	/// Present user with choice.
	pub struct DataDetected(ObjectSubclass<imp::DataDetected>)
		@extends WizardPage, adw::NavigationPage, gtk::Widget,
		@implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl DataDetected {
	pub fn new(disk: &Disk) -> Self {
		let obj: Self = glib::Object::new();

		let disk_row = DiskRow::new(disk.clone());
		disk_row.set_activatable(false);

		let imp = obj.imp();
		imp.disk_row_group.add(&disk_row);

		imp.label.set_label(&format!(
			"\
				Data detected on <b>{}</b>. Setup can use the entire disk, \
				erasing all documents, software and data on the disk, or install \
				into available free space, preserving existing data.\
			",
			disk.name(),
		));

		obj
	}
}
