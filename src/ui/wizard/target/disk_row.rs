use {
	super::dialogs,
	crate::prelude::*,
	crate::{
		models::disk::{Disk, Outcome, Unusable},
		ui::wizard::target::data_detected::DataDetected,
	},
	glib::format_size,
	std::{cell::OnceCell, future::Future},
};

mod imp {
	use super::*;

	#[derive(Debug, Default, gtk::CompositeTemplate, glib::Properties)]
	#[properties(wrapper_type = super::DiskRow)]
	#[template(file = "src/ui/wizard/target/disk-row.blp")]
	pub struct DiskRow {
		#[property(
			get,
			set = Self::construct_disk,
			construct_only,
		)]
		disk: OnceCell<Disk>,

		#[template_child]
		warn_dropdown: TemplateChild<gtk::MenuButton>,
		#[template_child]
		warn_heading: TemplateChild<gtk::Label>,
		#[template_child]
		warn_body: TemplateChild<gtk::Label>,
		#[template_child]
		icon: TemplateChild<gtk::Image>,
	}

	#[glib::object_subclass]
	impl ObjectSubclass for DiskRow {
		const NAME: &'static str = "DntDiskRow";
		type Type = super::DiskRow;
		type ParentType = adw::ActionRow;

		fn class_init(class: &mut Self::Class) {
			class.bind_template();
		}

		fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
			obj.init_template();
		}
	}

	#[glib::derived_properties]
	impl ObjectImpl for DiskRow {
		fn constructed(&self) {
			self.parent_constructed();

			let obj = self.obj();
			obj.property_expression("disk")
				.chain_property::<Disk>("name")
				.bind(&*obj, "title", glib::Object::NONE);

			obj.property_expression("disk")
				.chain_property::<Disk>("icon")
				.bind(&*self.icon, "icon-name", glib::Object::NONE);
		}
	}
	impl WidgetImpl for DiskRow {}
	impl ListBoxRowImpl for DiskRow {}
	impl PreferencesRowImpl for DiskRow {}
	impl ActionRowImpl for DiskRow {
		fn activate(&self) {
			ActionRowImplExt::parent_activate(self);

			let parent = self
				.obj()
				.ancestor(adw::NavigationView::static_type())
				.expect("DiskRow doesn't have an `Adw.NavigationView` parent")
				.downcast::<adw::NavigationView>()
				.unwrap();

			match self.disk().outcome() {
				Outcome::HasFreeSpace { .. } => {
					let data_detected = DataDetected::new(self.disk());
					parent.push(&data_detected);
				}
				Outcome::NotEnoughFreeSpace => {
					self.begin_installation(dialogs::insufficient_free_space())
				}
				Outcome::UnsupportedLayout => {
					self.begin_installation(dialogs::unsupported_layout())
				}
				Outcome::IsFull => self.begin_installation(dialogs::disk_is_full()),
				Outcome::IsEmpty => self.begin_installation(dialogs::begin_installation()),
			}
		}
	}

	impl DiskRow {
		fn begin_installation(&self, dialog_fut: impl Future<Output = bool> + 'static) {
			glib::MainContext::default().spawn_local(glib::clone!(
				#[strong(rename_to = obj)]
				self.obj(),
				async move {
					let result = dialog_fut.await;
					let wizard = crate::utils::ancestor::<adw::NavigationView>(&obj)
						.expect("Wizard page outside wizard");

					wizard.push(&crate::ui::wizard::installation::Installation::new());
				}
			));
		}

		fn disk(&self) -> &Disk {
			self.disk
				.get()
				.expect("Property `disk` is marked as `construct` -> it has to be initialized")
		}

		fn construct_disk(&self, value: Disk) {
			self.set_by_outcome(&value, value.outcome());
			self.set_by_unusable(&value, value.unusable());

			// Set the structure field
			glib::property::PropertySet::set(&self.disk, value);
		}

		fn set_by_outcome(&self, disk: &Disk, outcome: Outcome) {
			let used_free = || {
				format!(
					"{} {} – {} {}",
					format_size(disk.used()),
					gettext("Used"),
					format_size(disk.free()),
					gettext("Free")
				)
			};

			let subtitle;
			match outcome {
				Outcome::HasFreeSpace { .. } => {
					subtitle = used_free();
				}
				Outcome::NotEnoughFreeSpace => {
					self.note_insufficient_free_space();
					subtitle = used_free();
				}
				Outcome::UnsupportedLayout => {
					subtitle = format!("{} {}", format_size(disk.size()), gettext("Overall"));
				}
				Outcome::IsFull => {
					self.note_full();
					subtitle = format!(
						"{} {} – {}",
						format_size(disk.used()),
						gettext("Used"),
						gettext("Full")
					);
				}
				Outcome::IsEmpty => {
					subtitle = format!(
						"{} – {} {}",
						gettext("Empty"),
						format_size(disk.free()),
						gettext("Free")
					);
				}
			}

			self.warn_dropdown.set_icon_name("dialog-warning-symbolic");
			self.warn_dropdown.add_css_class("destructive-color");
			self.obj().set_subtitle(&subtitle);
		}

		fn set_by_unusable(&self, _disk: &Disk, ununsable: Option<Unusable>) {
			let Some(unusable) = ununsable else { return };
			self.obj().set_activatable(false);

			match unusable {
				Unusable::CantFitImage => self.note_insufficient_free_space(),
				Unusable::UnsupportedMedia => self.note_unsupported_media(),
				Unusable::System => self.note_system(),
			}
		}

		fn note_system(&self) {
			self.warn_dropdown.set_visible(true);
			self.warn_heading.set_label(&gettext("System Drive"));
			self.warn_body.set_label(&gettext(
				"This disk contains currently running system and cannot be wiped.",
			));
		}

		fn note_full(&self) {
			self.warn_dropdown.set_visible(true);
			self.warn_heading.set_label(&gettext("Disk Is Full"));
			self.warn_body.set_label(&gettext(
				"The disk is full or has very limited free space available. Setup will only offer to use the entire disk during installation.",
			));
		}

		fn note_insufficient_free_space(&self) {
			self.warn_dropdown.set_visible(true);
			self.warn_heading
				.set_label(&gettext("Disk Has Insufficient Free Space"));
			self.warn_body.set_label(&gettext(
				"The disk does not have enough free space for an installation. Setup will only offer to use the entire disk during installation.",
			));
		}

		fn note_too_small(&self) {
			self.warn_dropdown.set_visible(true);
			self.warn_heading.set_label(&gettext("Disk Too Small"));
			self.warn_body.set_label(&gettext(
				"This disk cannot be used by Deploy NT to perform the installation as it requires at least :3 GB of available space.",
			)); // todo: needs more context
		}

		fn note_unsupported_media(&self) {
			self.warn_dropdown.set_visible(true);
			self.warn_heading.set_label(&gettext("Unsupported Media"));
			self.warn_body.set_label(&gettext(
				"Optical drives are not supported as a target for installation.",
			));
		}
	}
}

glib::wrapper! {
	pub struct DiskRow(ObjectSubclass<imp::DiskRow>)
		@extends adw::ActionRow, adw::PreferencesRow, gtk::ListBoxRow, gtk::Widget,
		@implements gtk::Accessible, gtk::Actionable, gtk::Buildable, gtk::ConstraintTarget;
}

impl DiskRow {
	pub fn new(disk: Disk) -> Self {
		glib::Object::builder().property("disk", disk).build()
	}
}
