// todo: Use templates, maybe? some reactoring?

use {
	crate::{app::App, prelude::*},
	adw::AlertDialog,
	futures_util::StreamExt,
	gtk::glib::GString,
	std::time::Duration,
};

const RESPONSE_CONTINUE: &str = "install";
const RESPONSE_CANCEL: &str = "cancel";

/// Begin Installation dialog
///
/// This dialog is shown if the disk setup is
/// satisfactory for the installer and simply prompts the user
/// if they would like to proceed.
pub async fn begin_installation() -> bool {
	let dialog = AlertDialog::builder()
		.heading(gettext("Begin Installation?"))
		.body(gettext(
			"\
				Once the installation begins, the changes to \
				the selected disk are irreversible. \
				Continue with installation?\
			",
		))
		.default_response(RESPONSE_CONTINUE)
		.close_response(RESPONSE_CANCEL)
		.build();

	dialog.add_response(RESPONSE_CANCEL, &gettext("Cancel"));
	dialog.add_response(RESPONSE_CONTINUE, &gettext("Install"));
	dialog.set_response_appearance(RESPONSE_CONTINUE, adw::ResponseAppearance::Suggested);

	present_dialog(dialog).await
}

/// Begin Installation Dialog
///
/// This dialog is shown after a choise was made to use
/// the entire disk after being presented with actions to handle
/// existing data.
pub async fn begin_installation_danger() -> bool {
	danger_dialog(
		gettext("Begin Installation?"),
		gettext(
			"Once the installation begins, the changes to the selected disk are irreversible. <b>Proceeding will fully erase all documents, software and ata from the disk.</b> Continue with installation?",
		),
	)
	.await
}

/// Insufficient Free Space
///
/// This dialog is shown if there isn't sufficient free space to
/// install the operating system.
pub async fn insufficient_free_space() -> bool {
	danger_dialog(
		gettext("Insufficient Free Space"),
		gettext(
			"The selected disk does not have enough free space to install the operating system to. <b>Proceeding will fully erase all documents, software and data from the disk.</b> Continue with installation?",
		),
	)
	.await
}

/// Unsupported layout
pub async fn unsupported_layout() -> bool {
	danger_dialog(
		gettext("Unsupported Layout"),
		gettext(
			"\
				Installer could not analyze the disk's contents. \
				It can contain unknown data or be completely empty.
				\
				<b>Proceeding will fully erase all documents, software and \
				data from the disk.</b> \
				Continue with installation?\
			",
		),
	)
	.await
}

/// Full Disk
///
/// This dialog is shown if there isn't sufficient free space to
/// install the operating system.
pub async fn disk_is_full() -> bool {
	danger_dialog(
		gettext("Disk is Full"),
		gettext(
			"The selected disk has no free space to install the operating system to. <b>Proceeding will fully erase all documents, software and data from the disk.</b> Continue with installation?",
		),
	)
	.await
}

async fn danger_dialog(heading: impl Into<GString>, body: impl Into<GString>) -> bool {
	let dialog = AlertDialog::builder()
		.heading(heading)
		.body(body)
		.body_use_markup(true)
		.default_response(RESPONSE_CANCEL)
		.close_response(RESPONSE_CANCEL)
		.build();

	dialog.add_response(RESPONSE_CANCEL, &gettext("Cancel"));
	dialog.add_response(RESPONSE_CONTINUE, "");

	dialog.set_response_enabled(RESPONSE_CONTINUE, false);
	dialog.set_response_appearance(RESPONSE_CONTINUE, adw::ResponseAppearance::Destructive);

	let fut_count = count_down(dialog.clone());
	let fut_dialog = present_dialog(dialog);

	let (_, response) = futures_util::future::join(fut_count, fut_dialog).await;
	response
}

async fn present_dialog(dialog: AlertDialog) -> bool {
	let parent = App::global_instance().main_window();
	let choice = dialog.choose_future(&parent).await;

	match choice.as_str() {
		RESPONSE_CONTINUE => true,
		RESPONSE_CANCEL => false,
		inval => unreachable!("Invalid response: `{inval}`"),
	}
}

async fn count_down(dialog: AlertDialog) {
	let mut timer = async_io::Timer::interval(Duration::from_secs(1));
	let label_start = gettext("Erase and Install");

	for count in (1..=3).rev() {
		dialog.set_response_label(RESPONSE_CONTINUE, &format!("{label_start} ({count})"));
		timer.next().await;
	}

	dialog.set_response_label(RESPONSE_CONTINUE, &label_start);
	dialog.set_response_enabled(RESPONSE_CONTINUE, true);
}
