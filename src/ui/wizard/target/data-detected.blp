using Gtk 4.0;
using Adw 1;

template $DntWizardTargetDataDetected : $DntWizardPage {
	title              : _("Data Detected");
	next-button-policy : "hidden";

	Box {
		orientation : vertical;	
		spacing     : 24;

		Label label {
			styles     ["body"]
			justify    : center;
			wrap       : true;
			wrap-mode  : word;
			use-markup : true;

			label: _(
				"Data detected on <b>UwU</b>. Setup can use the entire disk, erasing all documents, software and data on the disk, or install into available free space, preserving existing data."
			);
		}

		Adw.PreferencesGroup disk_row_group {}
		
		Adw.PreferencesGroup {
			$DntCheckRow {
				title    : _("Use Entire Disk");
				subtitle : _("Fully <b>erase all data</b> on disk and do a fresh installation.");

				action-name   : "data-detected.choice";
				action-target : "'wipe'";
			}

			$DntCheckRow {
				title    : _("Install to Free Space");
				subtitle : _("Use free space on the disk for installation, preserving data.");

				action-name   : "data-detected.choice";
				action-target : "'free-space'";
			}
		}

		Button button {
			styles    ["pill", "suggested-action"]
			sensitive : false;
			halign    : center;
			label     : _("Start Installation");

			clicked => $on_install_clicked() swapped;
		}
	}
}
