//! Select local installation source

use gio::{SimpleAction, SimpleActionGroup};
use glib::VariantTy;
use {
	crate::prelude::*,
	crate::{
		models::ImageChoice,
		ui::{
			widgets::{filebutton::SelectionType, CheckRow, FileButton},
			wizard::{
				utils::throw_toast_msg,
				wizard_page::{NextButtonPolicy, WizardPage},
			},
		},
		utils::default,
	},
	color_eyre::{
		eyre::{bail, OptionExt},
		Result,
	},
	std::{cell::OnceCell, num::NonZero},
	wimlib::Wim,
};

mod imp {
	use glib::property::PropertySet;

	use crate::ui::wizard::Wizard;

	use super::*;

	#[derive(Debug, Default, gtk::CompositeTemplate)]
	#[template(file = "src/ui/wizard/source-local.blp")]
	pub struct SourceLocal {
		selection: OnceCell<ImageChoice>,
		#[template_child]
		file_button: TemplateChild<FileButton>,
		#[template_child]
		source_image_group: TemplateChild<adw::PreferencesGroup>,
		#[template_child]
		images_list: TemplateChild<gtk::ListBox>,

		actions: SimpleActionGroup,
	}

	#[glib::object_subclass]
	impl ObjectSubclass for SourceLocal {
		const NAME: &'static str = "DntWizardSourceLocal";
		type Type = super::SourceLocal;
		type ParentType = WizardPage;

		fn class_init(class: &mut Self::Class) {
			crate::ui::widgets::CheckRow::ensure_type();
			crate::ui::widgets::FileButton::ensure_type();

			class.bind_template();
			class.bind_template_callbacks();
		}

		fn instance_init(obj: &gtk::glib::subclass::InitializingObject<Self>) {
			obj.init_template();
		}
	}

	impl ObjectImpl for SourceLocal {
		fn constructed(&self) {
			self.parent_constructed();
			let obj = self.obj();

			// -- Setup actions --

			// Type action; Source type: Folder | File
			let type_action = SimpleAction::new_stateful(
				"type",
				Some(VariantTy::STRING),
				&SelectionType::Folder.to_variant(),
			);

			// Button switches
			type_action
				.bind_property("state", &*self.file_button, "selection-type")
				.sync_create()
				.transform_to(|_, value| SelectionType::from_variant(&value))
				.build();

			// Reset selection on type change
			type_action.connect_change_state(glib::clone!(
				#[strong]
				obj,
				move |self_, state| {
					obj.imp().reset_image_state();

					let state_variant = state.expect("This action is supposed to have state");
					self_.set_state(state_variant);
				}
			));

			// Image index
			let image_action =
				SimpleAction::new_stateful("image", Some(VariantTy::UINT32), &0_u32.to_variant());

			// If selected, we can continue
			image_action.connect_change_state(glib::clone!(
				#[strong]
				obj,
				move |self_, state| {
					let state_variant = state.expect("This action is supposed to have state");
					let state = state_variant
						.get::<u32>()
						.expect("Expected `u32` variant type");

					let next_button_policy = if state != 0 {
						NextButtonPolicy::Enabled
					} else {
						NextButtonPolicy::Disabled
					};

					obj.upcast_ref::<WizardPage>()
						.set_next_button_policy(next_button_policy);

					obj.upcast_ref::<Self::Type>()
						.imp()
						.selection
						.get()
						.unwrap()
						.set_image(NonZero::new(state));

					self_.set_state(state_variant);
				}
			));

			self.actions.add_action(&type_action);
			self.actions.add_action(&image_action);

			self.obj()
				.insert_action_group("source-local", Some(&self.actions));
		}
	}

	impl WidgetImpl for SourceLocal {
		fn root(&self) {
			self.parent_root();

			PropertySet::set(
				&self.selection,
				crate::utils::ancestor::<Wizard>(&*self.obj())
					.expect("Expected to be inside Wizard")
					.image_choice(),
			);
		}
	}

	impl NavigationPageImpl for SourceLocal {}

	#[gtk::template_callbacks]
	impl SourceLocal {
		#[template_callback]
		fn on_file_selected(&self) {
			// If no file was chosen
			let Some(file) = self.file_button.selected_file() else {
				return;
			};

			let result = match self
				.actions
				.action_state("type")
				.expect("Action `type` should have a state")
				.str()
				.expect("Expected string")
			{
				"folder" => self.select_wim_from_folder(file),
				"file" => self.select_wim_from_file(file),
				other => unreachable!("Unknown source type: {other}"),
			};

			let wim = match result {
				Ok(wim) => wim,
				Err(err) => {
					throw_toast_msg(&err.to_string());
					self.file_button.add_css_class("error");
					return;
				}
			};

			self.enumerate_images(&wim);
			self.selection.get().unwrap().set_wim(wim);
		}
	}

	impl SourceLocal {
		/// Clear image list and selection
		fn reset_image_state(&self) {
			self.images_list.remove_all();
			self.source_image_group.set_visible(false);

			self.actions
				.activate_action("image", Some(&0_u32.to_variant()));

			self.file_button.remove_css_class("error");

			self.obj()
				.upcast_ref::<WizardPage>()
				.set_next_button_policy(NextButtonPolicy::Disabled);
		}

		/// Enumerate images from WIM and put them in the list
		fn enumerate_images(&self, wim: &Wim) {
			self.reset_image_state();
			let image_count = wim.info().image_count;

			if image_count > 0 {
				self.source_image_group.set_visible(true);
			}

			for index in 1..=image_count {
				let index = NonZero::new(index).expect("Value can't be zero");
				let image = wim.select_image(index);
				let row = CheckRow::default();

				// Name: There *should* be some, otherwise by index
				if let Some(name) = image.property(wimlib::tstr!("NAME")) {
					row.set_title(&name.to_str());
				} else {
					let title = format!("Unnamed image {index}");
					row.set_title(&title);
				}

				// Optional description
				if let Some(description) = image.property(wimlib::tstr!("DESCRIPTION")) {
					row.set_title(&description.to_str());
				}

				row.set_action_name(Some("source-local.image"));
				row.set_action_target(Some(index.get()));

				self.images_list.append(&row);
			}
		}

		/// Select WIM from WIM / ESD file
		fn select_wim_from_file(&self, file: gio::File) -> Result<Wim> {
			let attempt = crate::utils::wim::try_open_wim(&default(), &file);
			attempt.and_then(|option| option.ok_or_eyre("Failed to open file"))
		}

		/// Look for WIM in a directory (sources/install.{wim,esd})
		fn select_wim_from_folder(&self, folder: gio::File) -> Result<Wim> {
			let wimlib = wimlib::WimLib::default();

			// First, try install.wim
			let install_wim = crate::utils::wim::try_open_wim(
				&wimlib,
				&folder.resolve_relative_path("sources/install.wim"),
			)?;

			// Or try…
			let wim = match install_wim {
				Some(wim) => wim,
				None => {
					// …install.esd
					let install_esd = crate::utils::wim::try_open_wim(
						&wimlib,
						&folder.resolve_relative_path("sources/install.wim"),
					)?;

					let Some(wim) = install_esd else {
						bail!("No installation image found in selected directory");
					};

					wim
				}
			};

			Ok(wim)
		}
	}
}

glib::wrapper! {
	/// Local source selection wizard page
	pub struct SourceLocal(ObjectSubclass<imp::SourceLocal>)
		@extends WizardPage, adw::NavigationPage, gtk::Widget,
		@implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}
