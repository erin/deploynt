//! Main page, choose between install or recovery

use crate::prelude::*;

mod imp {
	use super::*;

	#[derive(Debug, Default, gtk::CompositeTemplate)]
	#[template(file = "src/ui/initial-view.blp")]
	pub struct InitialView {
		#[template_child]
		status_page: gtk::TemplateChild<adw::StatusPage>,
	}

	#[glib::object_subclass]
	impl ObjectSubclass for InitialView {
		const NAME: &'static str = "DntInitialView";
		type Type = super::InitialView;
		type ParentType = adw::Bin;

		fn class_init(class: &mut Self::Class) {
			class.bind_template();
		}

		fn instance_init(obj: &gtk::glib::subclass::InitializingObject<Self>) {
			obj.init_template();
		}
	}

	impl ObjectImpl for InitialView {
		fn constructed(&self) {
			self.parent_constructed();
			self.status_page.set_icon_name(Some(crate::consts::APP_ID));
		}
	}

	impl WidgetImpl for InitialView {}
	impl BinImpl for InitialView {}
}

glib::wrapper! {
	pub struct InitialView(ObjectSubclass<imp::InitialView>)
		@extends gtk::Widget,
		@implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}
