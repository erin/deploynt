//! Application's main window

use crate::prelude::*;
use crate::{app::App, ui};

mod imp {
	use super::*;

	#[derive(Debug, Default, gtk::CompositeTemplate)]
	#[template(file = "src/ui/app-window.blp")]
	pub struct AppWindow {
		#[template_child]
		pub toast_overlay: TemplateChild<adw::ToastOverlay>,
		#[template_child]
		pub nav_view: TemplateChild<adw::NavigationView>,
	}

	#[glib::object_subclass]
	impl ObjectSubclass for AppWindow {
		const NAME: &'static str = "DntAppWindow";
		type Type = super::AppWindow;
		type ParentType = adw::ApplicationWindow;

		fn class_init(class: &mut Self::Class) {
			ui::initial_view::InitialView::ensure_type();
			ui::wizard::Wizard::ensure_type();
			ui::headerbar::HeaderBar::ensure_type();

			class.bind_template();
		}

		fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
			obj.init_template();
		}
	}

	impl ObjectImpl for AppWindow {
		fn constructed(&self) {
			self.parent_constructed();
			let obj = self.obj();

			if crate::consts::PROFILE == "dev" {
				obj.add_css_class("devel");
			}
		}
	}

	impl WidgetImpl for AppWindow {}
	impl WindowImpl for AppWindow {}
	impl ApplicationWindowImpl for AppWindow {}
	impl AdwApplicationWindowImpl for AppWindow {}
}

glib::wrapper! {
	pub struct AppWindow(ObjectSubclass<imp::AppWindow>)
		@extends adw::ApplicationWindow, gtk::ApplicationWindow, gtk::Window, gtk::Widget,
		@implements gio::ActionMap, gio::ActionGroup, gtk::Accessible, gtk::Buildable,
					gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}

impl AppWindow {
	pub fn new(app: &App) -> Self {
		glib::Object::builder().property("application", app).build()
	}

	pub fn toast_overlay(&self) -> adw::ToastOverlay {
		self.imp().toast_overlay.clone()
	}
}
