//! Adwaita application subclass

use {
	crate::{consts, prelude::*, ui::app_window::AppWindow},
	glib::WeakRef,
	gtk::gio::ActionEntry,
};

mod imp {
	use super::*;

	#[derive(Default)]
	pub struct App {
		pub main_window: WeakRef<AppWindow>,
		wimlib: wimlib::WimLib,
	}

	#[glib::object_subclass]
	impl ObjectSubclass for App {
		const NAME: &'static str = "DntApp";
		type Type = super::App;
		type ParentType = adw::Application;
	}

	impl ObjectImpl for App {}

	impl ApplicationImpl for App {
		fn activate(&self) {
			debug!("App:activate");
			self.parent_activate();

			let main_window = self.main_window.upgrade().unwrap_or_else(|| {
				let main_window = AppWindow::new(&self.obj());
				self.main_window.set(Some(&main_window));
				main_window
			});

			main_window.present()
		}

		fn startup(&self) {
			debug!("App::startup");
			self.parent_startup();
			self.obj().setup_actions();

			gtk::Window::set_default_icon_name(consts::APP_ID);
		}
	}

	impl GtkApplicationImpl for App {}
	impl AdwApplicationImpl for App {}
}

glib::wrapper! {
	pub struct App(ObjectSubclass<imp::App>)
		@extends adw::Application, gtk::Application, gio::Application,
		@implements gio::ActionMap, gio::ActionGroup;
}

impl App {
	/// Create a new instance of [`App`]
	pub fn new() -> Self {
		glib::Object::builder()
			.property("application-id", consts::APP_ID)
			.property("resource-base-path", consts::RESOURCE_BASE_PATH)
			.build()
	}

	/// Get a global instance of [`App`]
	pub fn global_instance() -> Self {
		gio::Application::default()
			.expect("Application is not initialized")
			.downcast()
			.expect("Application is a wrong subclass")
	}

	/// Get main window, panic if not present
	pub fn main_window(&self) -> AppWindow {
		self.main_window_option()
			.expect("No main window found in application")
	}

	/// Get main window
	pub fn main_window_option(&self) -> Option<AppWindow> {
		self.imp().main_window.upgrade()
	}

	/// Get global application instance
	pub fn current() -> Self {
		gio::Application::default()
			.expect("No application found")
			.downcast()
			.expect("Wrong application class")
	}

	/// Setup actions
	fn setup_actions(&self) {
		// Close window and quit application
		let quit = ActionEntry::builder("quit")
			.activate(move |self_: &Self, _, _| {
				self_.main_window().close();
				self_.quit();
			})
			.build();

		// About dialog
		let about = ActionEntry::builder("about")
			.activate(move |self_: &Self, _, _| {
				self_.show_about_dialog();
			})
			.build();

		self.add_action_entries([quit, about]);

		// Set shortcuts (accels)
		self.set_accels_for_action("app.quit", &["<Control>q"]);
		self.set_accels_for_action("window.close", &["<Control>w"]);
	}

	/// Show »About Deploy NT« dialog
	fn show_about_dialog(&self) {
		const EUPL_1_2_NOTICE: &str = "\
			This application comes with absolutely no warranty. \
			See the <a href=\"https://opensource.org/license/eupl-1-2\">European Union Public License, version 1.2</a> \
			or later for details.\
		";

		let developers = env!("CARGO_PKG_AUTHORS").split(',').collect::<Vec<_>>();

		let dialog = adw::AboutDialog::builder()
			.application_icon(consts::APP_ID)
			.application_name(gettext("Deploy NT"))
			.version(consts::VERSION)
			.license(EUPL_1_2_NOTICE)
			.copyright("Copyright © 2024 Erin")
			.developers(&developers[..])
			.designers(&developers[..])
			.build();

		dialog.add_legal_section(
			const_str::concat!("hivex ", hivex::VERSION),
			Some("Copyright © 2009-2022 Red Hat Inc."),
			gtk::License::Lgpl21,
			None,
		);

		dialog.add_legal_section(
			"NTFS-3G",
			Some("Copyright © 2022 Tuxera"),
			gtk::License::Gpl20,
			None,
		);

		{
			let (major, minor, patch) = wimlib::version();
			dialog.add_legal_section(
				&format!("wimlib {major}.{minor}.{patch}"),
				Some("Copyright © 2012-2023 Eric Biggers"),
				gtk::License::Lgpl30,
				None,
			);
		}

		dialog.present(Some(&self.main_window()));
	}
}
