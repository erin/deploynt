// pub const MESON_BUILD_ROOT: &str = env!("MESON_BUILD_ROOT");

/// Application ID, e.g. cz.erindesu.DeployNT
pub const APP_ID: &str = env!("APP_ID");
/// Directory with locales
pub const LOCALE_DIR: &str = env!("LOCALE_DIR");
/// Build profile (like dev or relese)
pub const PROFILE: &str = env!("PROFILE");
/// Package version
pub const VERSION: &str = env!("VERSION"); // (HACK: when parsing Cargo.toml complete, replace)
/// Package name
pub const PACKAGE_NAME: &str = env!("CARGO_PKG_NAME");
/// GResources base path
pub const RESOURCE_BASE_PATH: &str = "/cz/erindesu/DeployNT";
