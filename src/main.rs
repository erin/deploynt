// Everyday We Stray Further From God's Light

mod app;
mod consts;
mod models;
mod prelude;
mod ui;
mod utils;

use {
	adw::{gio, glib},
	color_eyre::Result,
	gtk::prelude::ApplicationExtManual,
};

static GRESOURCE_BYTES: &[u8] = with_builtin_macros::with_builtin! {
	let $dir = concat!(env!("CARGO_MANIFEST_DIR"), "/data/resources") in {
		gvdb_macros::include_gresource_from_dir!("/cz/erindesu/DeployNT", $dir)
	}
};

fn main() -> Result<()> {
	// Set localisation
	gettextrs::setlocale(gettextrs::LocaleCategory::LcAll, "");
	gettextrs::bindtextdomain(consts::PACKAGE_NAME, consts::LOCALE_DIR)?;
	gettextrs::textdomain(consts::PACKAGE_NAME)?;

	gio::resources_register(&gio::Resource::from_data(&glib::Bytes::from_static(
		GRESOURCE_BYTES,
	))?);

	// Print info about software versions
	prelude::info!("Deploy NT ({})", consts::APP_ID);
	prelude::info!("Version: {} ({})", consts::VERSION, consts::PROFILE);
	prelude::info!("Using hivex {}", hivex::VERSION);
	{
		let (major, minor, patch) = wimlib::version();
		prelude::info!("Using wimlib {major}.{minor}.{patch}");
	}

	app::App::new().run();
	Ok(())
}
