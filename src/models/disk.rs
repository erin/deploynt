use {
	adw::glib::{self, prelude::*, subclass::prelude::*},
	color_eyre::{eyre::OptionExt, Result},
	futures_util::{StreamExt, TryStreamExt},
	glib::Properties,
	std::cell::{Cell, OnceCell},
	udisks2::{block::BlockProxy, drive::DriveProxy},
	zbus::zvariant::OwnedObjectPath,
};

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Outcome {
	HasFreeSpace { offset: u64 },
	NotEnoughFreeSpace,
	UnsupportedLayout,
	IsFull,
	IsEmpty,
}

impl Outcome {
	pub const fn mandate_wipe_hint(&self) -> bool {
		matches!(
			self,
			Self::NotEnoughFreeSpace | Self::UnsupportedLayout | Self::IsFull
		)
	}
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Unusable {
	CantFitImage,
	UnsupportedMedia,
	System,
}

mod imp {
	use super::*;

	#[derive(Debug, Default, Properties)]
	#[properties(wrapper_type = super::Disk)]
	pub struct Disk {
		#[property(get, construct_only)]
		name: OnceCell<Box<str>>,
		#[property(get, construct_only)]
		used: Cell<u64>,
		#[property(get, construct_only)]
		free: Cell<u64>,
		#[property(get, construct_only)]
		size: Cell<u64>,
		#[property(get, construct_only)]
		system: Cell<bool>,
		#[property(get, construct_only)]
		icon: OnceCell<Box<str>>,

		pub(super) udisks_path: OnceCell<OwnedObjectPath>,
		pub(super) outcome: OnceCell<Outcome>,
		pub(super) unusable: OnceCell<Unusable>,
	}

	#[glib::object_subclass]
	impl ObjectSubclass for Disk {
		const NAME: &'static str = "DntDisk";
		type Type = super::Disk;
	}

	#[glib::derived_properties]
	impl ObjectImpl for Disk {}
}

glib::wrapper! {
	pub struct Disk(ObjectSubclass<imp::Disk>);
}

impl Disk {
	pub async fn new(
		ud2_client: &udisks2::Client,
		drive: &DriveProxy<'_>,
		required_space: u64,
	) -> Result<Self> {
		DiskBuilder::new(ud2_client, drive, required_space)
			.build()
			.await
	}

	pub fn outcome(&self) -> Outcome {
		*self
			.imp()
			.outcome
			.get()
			.expect("in built `DntDisk` object, there should be an initialized `outcome`")
	}

	pub fn unusable(&self) -> Option<Unusable> {
		self.imp().unusable.get().copied()
	}

	pub fn udisks_path(&self) -> &OwnedObjectPath {
		&self
			.imp()
			.udisks_path
			.get()
			.expect("in built `DntDisk` object, there should be an initialized `udisks_path`")
	}
}

struct DiskBuilder<'a> {
	ud2_client: &'a udisks2::Client,
	drive: &'a DriveProxy<'a>,
	outcome: Option<Outcome>,
	unusable: Option<Unusable>,
	required_space: u64,

	size: u64,
	used: u64,
	free: u64,
	icon_name: &'static str,
}

impl<'a> DiskBuilder<'a> {
	fn new(
		ud2_client: &'a udisks2::Client,
		drive: &'a DriveProxy<'a>,
		required_space: u64,
	) -> Self {
		Self {
			ud2_client,
			drive,
			required_space,
			outcome: None,
			unusable: None,
			size: 0,
			used: 0,
			free: 0,
			icon_name: " dialog-question-symbolic",
		}
	}

	// todo: guards?

	fn set_outcome(&mut self, outcome: Outcome) {
		self.outcome = Some(outcome);
	}

	fn set_unusable(&mut self, outcome: Unusable) {
		self.unusable = Some(outcome);
	}

	async fn build(mut self) -> Result<Disk> {
		let block_dev = crate::utils::disks::get_block_for_drive(&self.ud2_client, self.drive)
			.await?
			.ok_or_eyre("No block device found for selected disk")?;

		self.size = block_dev.size().await?;
		let name = [
			&self.drive.vendor().await?[..],
			&self.drive.model().await?,
			&glib::format_size(self.size),
		]
		.join(" ")
		.trim()
		.to_owned()
		.into_boxed_str(); // fixme: eww

		self.detect_type().await?;
		self.usage_analysis(&block_dev).await?;

		if self.size < self.required_space && self.unusable.is_none() {
			self.set_unusable(Unusable::CantFitImage);
		}

		let outcome = self.outcome.expect("Outcome has to be set at this point");
		let is_system = block_dev.hint_system().await?;
		if outcome.mandate_wipe_hint() && is_system {
			self.set_unusable(Unusable::System);
		}

		// Make object
		let object: Disk = glib::Object::builder()
			.property("name", name)
			.property("used", self.used)
			.property("size", self.size)
			.property("free", self.free)
			.property("icon", self.icon_name.to_owned().into_boxed_str())
			.property("system", is_system)
			.build();

		let imp = object.imp();

		imp.udisks_path
			.set(block_dev.into_inner().path().clone().into())
			.unwrap();

		imp.outcome.set(outcome).unwrap();

		if let Some(unusable) = self.unusable {
			imp.unusable.set(unusable).unwrap()
		};

		Ok(object)
	}

	async fn detect_type(&mut self) -> Result<()> {
		let name = kiam::when! {
			self.drive.optical().await? => {
				self.set_unusable(Unusable::UnsupportedMedia);
				"media-optical-symbolic"
			},
			self.drive.media_removable().await? => "usb-stick-symbolic",
			self.drive.rotation_rate().await? != 0 => "drive-harddisk-symbolic",
			_ => "drive-harddisk-solidstate-symbolic",
		};

		self.icon_name = name;
		Ok(())
	}

	async fn usage_analysis(&mut self, block_dev: &BlockProxy<'_>) -> Result<()> {
		let obj = self.ud2_client.object(block_dev.inner().path().clone())?;

		// Does contain a partition table?
		let Ok(table) = obj.partition_table().await else {
			let outcome = if obj.filesystem().await.is_ok() {
				Outcome::IsFull
			} else {
				Outcome::UnsupportedLayout
			};

			self.set_outcome(outcome);
			self.free = 0;
			return Ok(());
		};

		// From GNOME Disk Utility:
		// Free space = If there is at least this much slack between
		// partitions (currently 1% of the disk, but at most 1 MiB)
		let free_space_slack = (self.size / 100).min(1024 * 1024);

		let partitions = table.partitions().await?;
		let mut layouts: Vec<_> = {
			let ud2_client = self.ud2_client.clone();
			futures_util::stream::iter(partitions)
				.then(|partition| async move {
					let part = ud2_client.object(partition)?.partition().await?;
					let offset = part.offset().await?;
					let size = part.size().await?;

					Ok::<_, zbus::Error>((offset, size))
				})
				.try_collect()
				.await?
		};

		layouts.sort_by_key(|(offset, _)| *offset);
		layouts.push((self.size, 0)); // For remaining space

		let (
			// End of previous partition
			mut prev_end,
			// Used by partitions
			mut used,
			// Chosen free space offset from start
			mut free_space_offset,
			// Size of free space
			mut free_space_size,
			// We have multiple free spaces, that's not supported
			// by autoparter
			mut too_many_free_spaces,
		) = (0, 0, 0, 0, false);

		for (offset, size) in layouts {
			used += size;

			if too_many_free_spaces {
				continue;
			}

			let gap_size = offset.saturating_sub(prev_end);

			// Is this relevant to count as free space?
			if gap_size > free_space_slack {
				// In case there is no free space
				// detected previously
				if free_space_size == 0 {
					free_space_size = gap_size;
					free_space_offset = prev_end;
				} else {
					// Unsupported layout, holey disk.
					too_many_free_spaces = true;
				}
			}

			prev_end = offset + size;
		}

		let outcome = kiam::when! {
			too_many_free_spaces         => Outcome::UnsupportedLayout,
			free_space_size == 0         => Outcome::IsFull,
			free_space_size == self.size => Outcome::IsEmpty,
			_ => Outcome::HasFreeSpace { offset: free_space_offset },
		};

		self.set_outcome(outcome);
		self.free = free_space_size;
		self.used = used;

		Ok(())
	}
}
