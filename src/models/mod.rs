pub mod disk;
mod image_choice;

pub use image_choice::ImageChoice;

use crate::prelude::*;

#[derive(Clone, Copy, Debug, PartialEq, Eq, glib::Enum, glib::Variant)]
#[variant_enum(enum)]
#[enum_type(name = "DntPartitioningChoice")]
pub enum PartitioningChoice {
	FreeSpace,
	Wipe,
}
