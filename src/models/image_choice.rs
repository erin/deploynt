//! Windows installation choices

use {
	adw::{
		glib,
		glib::{prelude::*, subclass::prelude::*},
	},
	std::cell::{Cell, RefCell},
	wimlib::Wim,
};

mod imp {
	use super::*;

	#[derive(Default, glib::Properties)]
	#[properties(wrapper_type = super::ImageChoice)]
	pub struct ImageChoice {
		pub(crate) wim: RefCell<Option<wimlib::Wim>>,
		#[property(get, set, nullable)]
		image: Cell<Option<wimlib::ImageIndex>>,
	}

	#[glib::object_subclass]
	impl ObjectSubclass for ImageChoice {
		const NAME: &'static str = "DntInstallModel";
		type Type = super::ImageChoice;
		type ParentType = glib::Object;
	}

	#[glib::derived_properties]
	impl ObjectImpl for ImageChoice {}

	impl ImageChoice {
		pub fn set_wim(&self, wim: Wim) {
			*self.wim.borrow_mut() = Some(wim);
			self.obj().set_image(None::<wimlib::ImageIndex>);
		}
	}
}

glib::wrapper! {
	pub struct ImageChoice(ObjectSubclass<imp::ImageChoice>);
}

impl Default for ImageChoice {
	fn default() -> Self {
		glib::Object::new()
	}
}

impl ImageChoice {
	/// Set WIM in use
	pub fn set_wim(&self, wim: Wim) {
		self.imp().set_wim(wim);
	}

	/// Get current WIM
	pub fn wim(&self) -> std::cell::Ref<Option<Wim>> {
		self.imp().wim.borrow()
	}
}
